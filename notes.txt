What is a Data Model?

	A data model describes how data is organized and grouped in a database.

	By creating data models, we can anticipate which data will be managed by the database management system in accordance to the application being developed.

Data Modelling Scenario

	//Database should have a purpose and its organization should be related to the kind of application you are creating.

	//Scenario:

		Type: Course Booking System
		Description: Student/User should be able to book into a course.

		Features:

			-user registration
			-user authentication/login
			-Features Regular Logged In Users:
				-view available courses
				-enroll in a course
				-update their own details
				-delete their optional details (except for credentials)
			-Features Admin User
				-create courses
				-update courses
				archive/deactivate courses
				-re-activate courses
				-view all courses (active/inactive)

		Models:

		users
			{
				(id) - unique identifier for a document/entry
				entry
				username,
				firstName,
				lastName,
				email,
				password,
				isAdmin,
				grades,
				address,
				mobileNo.,
				enrollments: [
					{
						course_id,
						course_name,
						dateEnrolled,
						status,
						isPaid
					}
				]
			}

			user documents in your database should be uniformed and should be following a single model for organization of data.

			{
				username: "jeffrey1",
				email: "jeff@gmail.com"
				password: "1991jeff"
			},
			{
				username: "jenniekim",
				password: "jennieBP",
				brand: "Toyota"
			}

		courses